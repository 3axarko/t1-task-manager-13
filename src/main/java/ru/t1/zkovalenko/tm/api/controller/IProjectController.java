package ru.t1.zkovalenko.tm.api.controller;

import ru.t1.zkovalenko.tm.model.Project;

public interface IProjectController {

    void createProject();

    void clearProjects();

    void showProjects();

    void showProject(Project project);

    void showProjectById();

    void showProjectByIndex();

    void removeProjectById();

    void removeProjectByIndex();

    void updateProjectById();

    void updateProjectByIndex();

    void startProjectById();

    void startProjectByIndex();

    void completeProjectById();

    void completeProjectByIndex();

    void changeStatusById();

    void changeStatusByIndex();
}
