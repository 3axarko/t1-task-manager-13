package ru.t1.zkovalenko.tm.api.controller;

import ru.t1.zkovalenko.tm.model.Task;

import java.util.List;

public interface ITaskController {

    void createTask();

    void clearTasks();

    void showTasks();

    void showTask(Task task);

    void showTaskById();

    void showTaskByIndex();

    void removeTaskById();

    void removeTaskByIndex();

    void updateTaskById();

    void updateTaskByIndex();

    void startTaskById();

    void startTaskByIndex();

    void completeTaskById();

    void completeTaskByIndex();

    void changeStatusById();

    void changeStatusByIndex();

    void showTaskByProjectId();

    void renderTasks(List<Task> tasks);
}
