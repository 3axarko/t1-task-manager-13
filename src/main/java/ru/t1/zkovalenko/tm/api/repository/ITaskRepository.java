package ru.t1.zkovalenko.tm.api.repository;

import ru.t1.zkovalenko.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    Task add(Task project);

    void clear();

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    List<Task> findAllByProjectId(String projectId);

    Task removeById(String id);

    Task removeByIndex(Integer index);
}
