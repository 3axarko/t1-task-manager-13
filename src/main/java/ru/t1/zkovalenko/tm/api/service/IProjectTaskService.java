package ru.t1.zkovalenko.tm.api.service;

public interface IProjectTaskService {

    String bindTaskToProject(String projectId, String taskId);

    void removeProjectById(String projectId);

    String unbindTaskFromProject(String projectId, String taskId);

}
