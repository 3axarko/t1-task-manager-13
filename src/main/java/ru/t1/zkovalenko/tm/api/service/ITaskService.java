package ru.t1.zkovalenko.tm.api.service;

import ru.t1.zkovalenko.tm.enumerated.Status;
import ru.t1.zkovalenko.tm.model.Task;

import java.util.List;

public interface ITaskService {

    List<Task> findAll();

    Task add(Task project);

    Task create(String name);

    Task create(String name, String description);

    void clear();

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task changeTaskStatusById(String id, Status status);

    Task changeTaskStatusByIndex(Integer index, Status status);

    List<Task> findAllByProjectId(String projectId);
}
