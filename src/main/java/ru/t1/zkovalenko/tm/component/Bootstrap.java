package ru.t1.zkovalenko.tm.component;

import ru.t1.zkovalenko.tm.api.controller.ICommandController;
import ru.t1.zkovalenko.tm.api.controller.IProjectController;
import ru.t1.zkovalenko.tm.api.controller.IProjectTaskController;
import ru.t1.zkovalenko.tm.api.controller.ITaskController;
import ru.t1.zkovalenko.tm.api.repository.ICommandRepository;
import ru.t1.zkovalenko.tm.api.repository.IProjectRepository;
import ru.t1.zkovalenko.tm.api.repository.ITaskRepository;
import ru.t1.zkovalenko.tm.api.service.ICommandService;
import ru.t1.zkovalenko.tm.api.service.IProjectService;
import ru.t1.zkovalenko.tm.api.service.IProjectTaskService;
import ru.t1.zkovalenko.tm.api.service.ITaskService;
import ru.t1.zkovalenko.tm.constant.ArgumentsConst;
import ru.t1.zkovalenko.tm.constant.TerminalConst;
import ru.t1.zkovalenko.tm.controller.CommandController;
import ru.t1.zkovalenko.tm.controller.ProjectController;
import ru.t1.zkovalenko.tm.controller.ProjectTaskController;
import ru.t1.zkovalenko.tm.controller.TaskController;
import ru.t1.zkovalenko.tm.enumerated.Status;
import ru.t1.zkovalenko.tm.model.Project;
import ru.t1.zkovalenko.tm.repository.CommandRepository;
import ru.t1.zkovalenko.tm.repository.ProjectRepository;
import ru.t1.zkovalenko.tm.repository.TaskRepository;
import ru.t1.zkovalenko.tm.service.CommandService;
import ru.t1.zkovalenko.tm.service.ProjectService;
import ru.t1.zkovalenko.tm.service.ProjectTaskService;
import ru.t1.zkovalenko.tm.service.TaskService;
import ru.t1.zkovalenko.tm.util.TerminalUtil;

import java.util.Locale;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final IProjectController projectController = new ProjectController(projectService, projectTaskService);

    private final IProjectTaskController projectTaskController = new ProjectTaskController(projectTaskService);

    private void processArgument(final String arg) {
        switch (arg.toLowerCase(Locale.ENGLISH)) {
            case TerminalConst.VERSION:
            case ArgumentsConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.HELP:
            case ArgumentsConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.ABOUT:
            case ArgumentsConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.INFO:
            case ArgumentsConst.INFO:
                commandController.showInfo();
                break;
            case TerminalConst.ARGUMENTS:
            case ArgumentsConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.COMMANDS:
            case ArgumentsConst.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.showProjects();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case TerminalConst.PROJECT_SHOW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case TerminalConst.PROJECT_SHOW_BY_ID:
                projectController.showProjectById();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case TerminalConst.PROJECT_START_BY_ID:
                projectController.startProjectById();
                break;
            case TerminalConst.PROJECT_START_BY_INDEX:
                projectController.startProjectByIndex();
                break;
            case TerminalConst.PROJECT_COMPLETE_BY_ID:
                projectController.completeProjectById();
                break;
            case TerminalConst.PROJECT_COMPLETE_BY_INDEX:
                projectController.completeProjectByIndex();
                break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_ID:
                projectController.changeStatusById();
                break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_INDEX:
                projectController.changeStatusByIndex();
                break;
            case TerminalConst.TASK_LIST:
                taskController.showTasks();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalConst.TASK_SHOW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case TerminalConst.TASK_SHOW_BY_ID:
                taskController.showTaskById();
                break;
            case TerminalConst.TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case TerminalConst.TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case TerminalConst.TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case TerminalConst.TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case TerminalConst.TASK_START_BY_ID:
                taskController.startTaskById();
                break;
            case TerminalConst.TASK_START_BY_INDEX:
                taskController.startTaskByIndex();
                break;
            case TerminalConst.TASK_COMPLETE_BY_ID:
                taskController.completeTaskById();
                break;
            case TerminalConst.TASK_COMPLETE_BY_INDEX:
                taskController.completeTaskByIndex();
                break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_ID:
                taskController.changeStatusById();
                break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_INDEX:
                taskController.changeStatusByIndex();
                break;
            case TerminalConst.TASK_BIND_TO_PROJECT:
                projectTaskController.bindTaskToProject();
                break;
            case TerminalConst.TASK_UNBIND_FROM_PROJECT:
                projectTaskController.unbindTaskFromProject();
                break;
            case TerminalConst.TASK_SHOW_BY_PROJECT_ID:
                taskController.showTaskByProjectId();
                break;
            case TerminalConst.EXIT:
                exit();
            default:
                commandController.showError();
        }
    }

    private void exit() {
        System.exit(0);
    }

    public void run (String[] args) {
        if (processArguments(args)) exit();

        initDemoData();
        commandController.showWelcome();
        while (true) {
            System.out.println("Enter Command:");
            final String command = TerminalUtil.nextLine();
            System.out.println("---");
            processArgument(command);
        }
    }

    private boolean processArguments(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

    private void initDemoData(){
        projectService.add(new Project("project1", Status.IN_PROGRESS));
        projectService.add(new Project("project2", Status.NOT_STARTED));
        projectService.add(new Project("project3", Status.IN_PROGRESS));
        projectService.add(new Project("project4", Status.COMPLETED));

        taskService.create("task1");
        taskService.create("task2");
    }

}
