package ru.t1.zkovalenko.tm.controller;

import ru.t1.zkovalenko.tm.api.controller.ICommandController;
import ru.t1.zkovalenko.tm.api.service.ICommandService;
import ru.t1.zkovalenko.tm.model.Command;
import ru.t1.zkovalenko.tm.util.ColorizeConsoleTextUtil;
import ru.t1.zkovalenko.tm.util.FormatUtil;

public final class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showError() {
        ColorizeConsoleTextUtil.redText("Command Not Found");
    }

    @Override
    public void showArguments(){
        System.out.println("[ARGUMENTS]");
        final Command[] commands = commandService.getTerminalControls();
        for (final Command command: commands) {
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    @Override
    public void showCommands(){
        System.out.println("[COMMANDS]");
        final Command[] commands = commandService.getTerminalControls();
        for (final Command command: commands) {
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }


    @Override
    public void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.13.0");
    }

    @Override
    public void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Who is the best: Read below");
        System.out.println("Developer: 3axarko");
        System.out.println("And he's really good");
    }

    @Override
    public void showHelp() {
        System.out.println("[HELP]");
        System.out.println("\nType key or command name\n");
        final Command[] commands = commandService.getTerminalControls();
        for (final Command command: commands) System.out.println(command);
    }

    @Override
    public void showInfo() {
        System.out.println("Available processors (cores): " + Runtime.getRuntime().availableProcessors());

        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + FormatUtil.convertBytes(freeMemory));

        final long maxMemory = Runtime.getRuntime().maxMemory();
        String maxMemoryChecked = maxMemory == Long.MAX_VALUE ? "no limit" : FormatUtil.convertBytes(maxMemory);
        System.out.println("Maximum memory: " + maxMemoryChecked);

        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory: " + FormatUtil.convertBytes(totalMemory));

        System.out.println("Usage memory: " + FormatUtil.convertBytes(totalMemory - freeMemory));
    }

    @Override
    public void showWelcome(){
        System.out.println("** Welcome to Task-Manager **");
    }

}
