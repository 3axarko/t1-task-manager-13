package ru.t1.zkovalenko.tm.controller;

import ru.t1.zkovalenko.tm.api.controller.IProjectTaskController;
import ru.t1.zkovalenko.tm.api.service.IProjectTaskService;
import ru.t1.zkovalenko.tm.constant.ConsoleColorConst;
import ru.t1.zkovalenko.tm.util.ColorizeConsoleTextUtil;
import ru.t1.zkovalenko.tm.util.TerminalUtil;

public class ProjectTaskController implements IProjectTaskController {

    private final IProjectTaskService projectTaskService;

    public ProjectTaskController(final IProjectTaskService projectTaskService) {
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void bindTaskToProject(){
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID: ");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID: ");
        final String taskId = TerminalUtil.nextLine();
        final String error = projectTaskService.bindTaskToProject(projectId, taskId);
        if (error == null) ColorizeConsoleTextUtil.greenText("[OK]");
        else ColorizeConsoleTextUtil.redText(error);
    }

    @Override
    public void unbindTaskFromProject(){
        System.out.println("[UNBIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID: ");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID: ");
        final String taskId = TerminalUtil.nextLine();
        final String error = projectTaskService.unbindTaskFromProject(projectId, taskId);
        if (error == null) ColorizeConsoleTextUtil.greenText("[OK]");
        else ColorizeConsoleTextUtil.redText(error);
    }

}
