package ru.t1.zkovalenko.tm.model;

public final class Command {

    private String name = "";
    private String argument;
    private String description = "";

    public Command() {
    }

    public Command(String name, String argument, String description) {
        this.name = name;
        this.argument = argument;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArgument() {
        return argument;
    }

    public void setArgument(String argument) {
        this.argument = argument;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString(){
        Integer spacesArgs = 7;
        Integer spacesNames = 40;
        String result = "";
        if (argument != null && !argument.isEmpty()) result += argument;
        result += spaceCounter(argument, spacesArgs);
        if (name != null && !name.isEmpty()) result += name;
        result += spaceCounter(name, spacesNames);
        if (description != null && !description.isEmpty()) result += description;
        return result;
    }

    private String spaceCounter(String string, Integer maxSpace){
        if (string == null) string = "";
        Integer resultCountSpaces = maxSpace - string.length();
        resultCountSpaces = resultCountSpaces > 0 ? resultCountSpaces : 1;
        String resultSpaces = "";
        for (int i = 0; i < resultCountSpaces; i++){
            resultSpaces += " ";
        }
        return resultSpaces;
    }

}
