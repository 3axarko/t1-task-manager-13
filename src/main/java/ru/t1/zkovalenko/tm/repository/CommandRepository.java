package ru.t1.zkovalenko.tm.repository;

import ru.t1.zkovalenko.tm.api.repository.ICommandRepository;
import ru.t1.zkovalenko.tm.constant.ArgumentsConst;
import ru.t1.zkovalenko.tm.constant.TerminalConst;
import ru.t1.zkovalenko.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    private final static Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentsConst.VERSION,
            "Show app version"
    );

    private final static Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentsConst.ABOUT,
            "Who did it? And why?"
    );

    private final static Command HELP = new Command(
            TerminalConst.HELP, ArgumentsConst.HELP,
            "App commands"
    );

    private final static Command EXIT = new Command(
            TerminalConst.EXIT, null,
            "Quit from program"
    );

    private final static Command INFO = new Command(
            TerminalConst.INFO, ArgumentsConst.INFO,
            "Info about system"
    );

    private final static Command ARGUMENTS = new Command(
            TerminalConst.ARGUMENTS, ArgumentsConst.ARGUMENTS,
            "List of arguments"
    );

    private final static Command COMMANDS = new Command(
            TerminalConst.COMMANDS, ArgumentsConst.COMMANDS,
            "List of Available commands"
    );


    private final static Command PROJECT_CLEAR = new Command(
            TerminalConst.PROJECT_CLEAR, null,
            "Remove all projects"
    );

    private final static Command PROJECT_LIST = new Command(
            TerminalConst.PROJECT_LIST, null,
            "Display project list"
    );

    private final static Command PROJECT_CREATE = new Command(
            TerminalConst.PROJECT_CREATE, null,
            "Create new project"
    );

    private final static Command PROJECT_SHOW_BY_INDEX = new Command(
            TerminalConst.PROJECT_SHOW_BY_INDEX, null,
            "Display project by index"
    );

    private final static Command PROJECT_SHOW_BY_ID = new Command(
            TerminalConst.PROJECT_SHOW_BY_ID, null,
            "Display project by id"
    );

    private final static Command PROJECT_UPDATE_BY_INDEX = new Command(
            TerminalConst.PROJECT_UPDATE_BY_INDEX, null,
            "Update project by index"
    );

    private final static Command PROJECT_UPDATE_BY_ID = new Command(
            TerminalConst.PROJECT_UPDATE_BY_ID, null,
            "Update project by id"
    );

    private final static Command PROJECT_REMOVE_BY_INDEX = new Command(
            TerminalConst.PROJECT_REMOVE_BY_INDEX, null,
            "Remove project by index"
    );

    private final static Command PROJECT_REMOVE_BY_ID = new Command(
            TerminalConst.PROJECT_REMOVE_BY_ID, null,
            "Remove project by id"
    );

    private final static Command TASK_CLEAR = new Command(
            TerminalConst.TASK_CLEAR, null,
            "Remove all tasks"
    );

    private final static Command TASK_LIST = new Command(
            TerminalConst.TASK_LIST, null,
            "Display task list"
    );

    private final static Command TASK_CREATE = new Command(
            TerminalConst.TASK_CREATE, null,
            "Create new task"
    );

    private final static Command TASK_SHOW_BY_INDEX = new Command(
            TerminalConst.TASK_SHOW_BY_INDEX, null,
            "Display task by index"
    );

    private final static Command TASK_SHOW_BY_ID = new Command(
            TerminalConst.TASK_SHOW_BY_ID, null,
            "Display task by id"
    );

    private final static Command TASK_UPDATE_BY_INDEX = new Command(
            TerminalConst.TASK_UPDATE_BY_INDEX, null,
            "Update task by index"
    );

    private final static Command TASK_UPDATE_BY_ID = new Command(
            TerminalConst.TASK_UPDATE_BY_ID, null,
            "Update task by id"
    );

    private final static Command TASK_REMOVE_BY_INDEX = new Command(
            TerminalConst.TASK_REMOVE_BY_INDEX, null,
            "Remove task by index"
    );

    private final static Command TASK_REMOVE_BY_ID = new Command(
            TerminalConst.TASK_REMOVE_BY_ID, null,
            "Remove task by id"
    );

    private final static Command PROJECT_START_BY_ID = new Command(
            TerminalConst.PROJECT_START_BY_ID, null,
            "Project start by id"
    );

    private final static Command PROJECT_START_BY_INDEX = new Command(
            TerminalConst.PROJECT_START_BY_INDEX, null,
            "Project start by index"
    );

    private final static Command PROJECT_COMPLETE_BY_ID = new Command(
            TerminalConst.PROJECT_COMPLETE_BY_ID, null,
            "Project complete by id"
    );

    private final static Command PROJECT_COMPLETE_BY_INDEX = new Command(
            TerminalConst.PROJECT_COMPLETE_BY_INDEX, null,
            "Project complete by index"
    );

    private final static Command PROJECT_CHANGE_STATUS_BY_ID = new Command(
            TerminalConst.PROJECT_CHANGE_STATUS_BY_ID, null,
            "Project change by id"
    );

    private final static Command PROJECT_CHANGE_STATUS_BY_INDEX = new Command(
            TerminalConst.PROJECT_CHANGE_STATUS_BY_INDEX, null,
            "Project change by index"
    );

    private final static Command TASK_START_BY_ID = new Command(
            TerminalConst.TASK_START_BY_ID, null,
            "Task start by id"
    );

    private final static Command TASK_START_BY_INDEX = new Command(
            TerminalConst.TASK_START_BY_INDEX, null,
            "Task start by index"
    );

    private final static Command TASK_COMPLETE_BY_ID = new Command(
            TerminalConst.TASK_COMPLETE_BY_ID, null,
            "Task complete by id"
    );

    private final static Command TASK_COMPLETE_BY_INDEX = new Command(
            TerminalConst.TASK_COMPLETE_BY_INDEX, null,
            "Task complete by index"
    );

    private final static Command TASK_CHANGE_STATUS_BY_ID = new Command(
            TerminalConst.TASK_CHANGE_STATUS_BY_ID, null,
            "Task change by id"
    );

    private final static Command TASK_CHANGE_STATUS_BY_INDEX = new Command(
            TerminalConst.TASK_CHANGE_STATUS_BY_INDEX, null,
            "Task change by index"
    );

    private final static Command TASK_BIND_TO_PROJECT = new Command(
            TerminalConst.TASK_BIND_TO_PROJECT, null,
            "Bind task to project"
    );

    private final static Command TASK_UNBIND_FROM_PROJECT = new Command(
            TerminalConst.TASK_UNBIND_FROM_PROJECT, null,
            "Unbind task from project"
    );

    private final static Command TASK_SHOW_BY_PROJECT_ID = new Command(
            TerminalConst.TASK_SHOW_BY_PROJECT_ID, null,
            "Show task by project id"
    );


    private final static Command[] TERMINAL_CONTROLS = new Command[]{
            INFO, ABOUT, VERSION, HELP, COMMANDS, ARGUMENTS,
            PROJECT_CLEAR, PROJECT_LIST, PROJECT_CREATE,
            PROJECT_SHOW_BY_INDEX, PROJECT_SHOW_BY_ID, PROJECT_UPDATE_BY_INDEX,
            PROJECT_UPDATE_BY_ID, PROJECT_REMOVE_BY_INDEX, PROJECT_REMOVE_BY_ID,
            PROJECT_START_BY_ID, PROJECT_START_BY_INDEX, PROJECT_COMPLETE_BY_ID,
            PROJECT_COMPLETE_BY_INDEX, PROJECT_CHANGE_STATUS_BY_ID, PROJECT_CHANGE_STATUS_BY_INDEX,
            TASK_CLEAR, TASK_LIST, TASK_CREATE,
            TASK_SHOW_BY_INDEX, TASK_SHOW_BY_ID, TASK_UPDATE_BY_INDEX,
            TASK_UPDATE_BY_ID, TASK_REMOVE_BY_INDEX, TASK_REMOVE_BY_ID,
            TASK_START_BY_ID, TASK_START_BY_INDEX, TASK_COMPLETE_BY_ID,
            TASK_COMPLETE_BY_INDEX, TASK_CHANGE_STATUS_BY_ID, TASK_CHANGE_STATUS_BY_INDEX,
            TASK_BIND_TO_PROJECT, TASK_UNBIND_FROM_PROJECT, TASK_SHOW_BY_PROJECT_ID,
            EXIT
    };

    @Override
    public Command[] getTerminalControls(){
        return TERMINAL_CONTROLS;
    }
}
