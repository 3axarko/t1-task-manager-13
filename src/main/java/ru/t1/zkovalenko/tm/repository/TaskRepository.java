package ru.t1.zkovalenko.tm.repository;

import ru.t1.zkovalenko.tm.api.repository.ITaskRepository;
import ru.t1.zkovalenko.tm.model.Task;
import java.util.ArrayList;
import java.util.List;

public final class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public List<Task> findAll(){
        return tasks;
    }

    @Override
    public Task add(Task task){
        tasks.add(task);
        return task;
    }

    @Override
    public void clear(){
        tasks.clear();
    }

    @Override
    public Task findOneById(final String id){
        for (final Task task: tasks){
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Task findOneByIndex(final Integer index){
        return tasks.size() > index ? tasks.get(index) : null;
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId){
        List<Task> resultTasks = new ArrayList<>();
        for (final Task task: tasks){
            if (task.getProjectId() == null) continue;
            if (projectId.equals(task.getProjectId())) resultTasks.add(task);
        }
        return resultTasks;
    }

    @Override
    public Task removeById(final String id){
        final Task task = findOneById(id);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeByIndex(final Integer index){
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

}
