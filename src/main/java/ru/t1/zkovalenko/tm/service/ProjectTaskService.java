package ru.t1.zkovalenko.tm.service;

import ru.t1.zkovalenko.tm.api.repository.IProjectRepository;
import ru.t1.zkovalenko.tm.api.repository.ITaskRepository;
import ru.t1.zkovalenko.tm.api.service.IProjectTaskService;
import ru.t1.zkovalenko.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService{

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(
            final IProjectRepository projectRepository,
            final ITaskRepository taskRepository
    ){
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public String bindTaskToProject(final String projectId, final String taskId){
        if (projectId == null || projectId.isEmpty()) return "projectId is empty";
        if (taskId == null || taskId.isEmpty()) return "taskId is empty";
        if (!projectRepository.existById(projectId)) return "project not found";
        final Task task = taskRepository.findOneById(taskId);
        if (task == null) return "task not found";
        task.setProjectId(projectId);
        return null;
    }

    @Override
    public void removeProjectById(final String projectId){
        if (projectId == null || projectId.isEmpty()) return;
        if (!projectRepository.existById(projectId)) return;
        final List<Task> tasks = taskRepository.findAllByProjectId(projectId);
        for (final Task task : tasks) taskRepository.removeById(task.getId());
        projectRepository.removeById(projectId);
    }

    @Override
    public String unbindTaskFromProject(final String projectId, final String taskId){
        if (projectId == null || projectId.isEmpty()) return "projectId is empty";
        if (taskId == null || taskId.isEmpty()) return "taskId is empty";
        if (!projectRepository.existById(projectId)) return "project not found";
        final Task task = taskRepository.findOneById(taskId);
        if (task == null) return "task not found";
        task.setProjectId(null);
        return null;
    }

}
