package ru.t1.zkovalenko.tm.util;

import ru.t1.zkovalenko.tm.constant.ConsoleColorConst;

public interface ColorizeConsoleTextUtil {

    static String colorText(String inText, String color){
        return color + inText + ConsoleColorConst.RESET;
    }

    static void colorPrintln(String inText, String color){
        System.out.println(colorText(inText, color));
    }

    static void greenText(String string) { colorPrintln(string, ConsoleColorConst.GREEN); }

    static void redText(String string) { colorPrintln(string, ConsoleColorConst.RED); }

}
